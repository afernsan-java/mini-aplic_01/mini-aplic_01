/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UD_3_DI_Ejercicio_12_aplicacion_agregar_campos;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public class ClaseModeloTabla extends AbstractTableModel {
    
     private String[] columnNames = {"Nombre ", "Apellidos", "Edad", "Profesion", "Casado/a"};      
     private final ArrayList<ClasePersona> ListaPersonas = new ArrayList<>();
    
     public void agregarPersona(ClasePersona objPersona){
         ListaPersonas.add(objPersona);//añade una nueva ListaPersonas
         fireTableDataChanged();// Indica que se ha cambiado el estado de la tabla
     }
        
     public void eliminarPersona(int rowIndex) {
        ListaPersonas.remove(rowIndex);//borra el contenido que este en el numero de linea indicado en rowIndex
        fireTableDataChanged();// Indica que se ha cambiado
    }
     
     public void guardarPersona(int row, ClasePersona objPersona){
         ListaPersonas.remove(row);//borra el contenido que este en el numero de lnea indicado rowIndex
         ListaPersonas.add(row, objPersona);
         fireTableDataChanged();// Indica que se ha cambiado
     }
     
     public Object getPersona(int rowIndex) {       
        return ListaPersonas.get(rowIndex);
    }
  
    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }
    
    @Override
    public String getColumnName(int col) {         
        return columnNames[col];     
    } 
         
    @Override
    public int getRowCount() {
        return ListaPersonas.size();      
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;     
    }

    @Override
    public Object getValueAt(int row, int column) {
        
        switch(column){
            case 0: return ListaPersonas.get(row).getNombre();             
            case 1: return ListaPersonas.get(row).getApellidos();             
            case 2: return ListaPersonas.get(row).getEdad();             
            case 3: return ListaPersonas.get(row).getProfesion();              
            case 4: return ListaPersonas.get(row).getCasado();             
            default:return null;
        }         
    }    
    
    /*Este método es el que deberemos sobrescribir para cambiar el tipo de dato de las columnas*/     
    @Override
    public Class getColumnClass(int colIndex) { 
        return getValueAt(0, colIndex).getClass();     
    } 
          
    //Este método se utiliza para indicar las celdas que se pueden editar o no      */     
    @Override
    public boolean isCellEditable(int row, int col) {        
            return false;                     
    }
    
    /*      
    * Solo es necesario implementar este método si cambia el valor de la                                         
    * celda.      
    */     
//     @Override
//    public void setValueAt(Object value, int row, int col) {   
//        ClasePersona person = ListaPersonas.get(row);
//      switch(col){
//            case 0:  person.setNombre((String) value);
//            case 1:  person.setApellidos((String) value);             
//            case 2:  person.setEdad((int) value);             
//            case 3:  person.setProfesion((String) (value));              
//            case 4:  person.setCasado((boolean) (value)); 
//        } 
//        fireTableCellUpdated(row, col);     
//    }     

}
