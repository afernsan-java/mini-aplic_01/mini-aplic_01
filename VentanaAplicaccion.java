/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UD_3_DI_Ejercicio_12_aplicacion_agregar_campos;

import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * @author Abraham Fernandez Sanchez
 */
public class VentanaAplicaccion extends javax.swing.JFrame implements ActionListener{
          
     ClaseModeloTabla modelo = new ClaseModeloTabla();
 
    /**
     * Creates new form ClasePrincipalAgregarRegistros
     */
    public VentanaAplicaccion() {        
        initComponents();   
        setLocationRelativeTo(null);
        jTable1.setModel(modelo);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
        jButtonEditar.setVisible(true);
        jButtonAgregar.setVisible(true);        
        jButtonEliminar.setVisible(true);
        jButtonCancelar.setVisible(false);
        jButtonGuardar.setVisible(false);        
        if(jTable1.getSelectedRow()==-1){
            jButtonEditar.setVisible(false);
        }
        
        jPanel3.setVisible(false);//ocultar el panel de la tabla, lo visualizare cuando se meta el primer registro persona
    }

    public void agregarPersona(){        
        
        try {
        String nom = jTextFieldNombre.getText();
        String apell= jTextFieldApellidos.getText();
        int ed = Integer.parseInt(jTextFieldEdad.getText());
        String prof = jTextFieldProfesion.getText();
        Boolean cas=false;
            if(jComboBoxCasado.getSelectedIndex()==0){
                cas=true;
            }    
            if(nom.length()!=0 && apell.length()!=0 && prof.length()!=0){
                modelo.agregarPersona(new ClasePersona(nom, apell, ed, prof, cas));
            }else{
                JOptionPane.showMessageDialog(this, "Tienes que rellenar todos los campos obligatoriamente ");
            }  
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Se tiene que introducir una edad obligatoriamente ");            
        }       
      
        //------------------------------------------------------------
        //PONER EN BLANCO LOS JTEXT
        jTextFieldNombre.setText("");
        jTextFieldApellidos.setText("");
        jTextFieldEdad.setText("");
        jTextFieldProfesion.setText("");
        jComboBoxCasado.setSelectedIndex(0);
    }   
    
    public void eliminarPersona(){
        try {
            modelo.eliminarPersona(jTable1.getSelectedRow());
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this, "Tienes que seleccionar una persona para poder eleminarla");
        }        
    }    
    
    public void editarPersona(){
        try {
            
            int filaSelec =jTable1.getSelectedRow();  
            jTextFieldNombre.setText(modelo.getValueAt(filaSelec, 0).toString());
            jTextFieldApellidos.setText(modelo.getValueAt(filaSelec, 1).toString());
            jTextFieldEdad.setText(modelo.getValueAt(filaSelec, 2).toString());
            jTextFieldProfesion.setText(modelo.getValueAt(filaSelec, 3).toString());
            int cas=1;
                if(modelo.getValueAt(filaSelec, 4).equals(true)){
                    cas=0;
                }
            jComboBoxCasado.setSelectedIndex(cas);  
            
            jButtonAgregar.setVisible(false);
            jButtonEliminar.setVisible(false);
            jButtonEditar.setVisible(false);
            jButtonCancelar.setVisible(true);
            jButtonGuardar.setVisible(true);
            
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this, "Tienes que elegir una persona para poder editar sus datos");
        }
    }
    
    public void guardarPersona(){
    
        try {
            String nom = jTextFieldNombre.getText();
            String apell= jTextFieldApellidos.getText();
            int ed = Integer.parseInt(jTextFieldEdad.getText());
            String prof = jTextFieldProfesion.getText();
            Boolean cas=false;
                if(jComboBoxCasado.getSelectedIndex()==0){
                    cas=true;
                }    
                if(nom.length()!=0 && apell.length()!=0 && prof.length()!=0){
                    modelo.guardarPersona(jTable1.getSelectedRow(), new ClasePersona(nom, apell, ed, prof, cas));
                //------------------------------------------------------------
                    //PONER EN BLANCO LOS JTEXT
                    jTextFieldNombre.setText("");
                    jTextFieldApellidos.setText("");
                    jTextFieldEdad.setText("");
                    jTextFieldProfesion.setText("");
                    jComboBoxCasado.setSelectedIndex(0);
                
                }else{
                    JOptionPane.showMessageDialog(this, "No puedes dejar ningun campo vacio ");
                }  
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Se tiene que introducir una edad obligatoriamente ");            
        }    
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldApellidos = new javax.swing.JTextField();
        jTextFieldEdad = new javax.swing.JTextField();
        jTextFieldProfesion = new javax.swing.JTextField();
        jComboBoxCasado = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButtonAgregar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jButtonGuardar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jComboBoxCasado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SI", "NO" }));

        jLabel1.setText("Nombre:");

        jLabel2.setText("Apellidos:");

        jLabel3.setText("Edad:");

        jLabel4.setText("Profesion:");

        jLabel5.setText("Casado:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTextFieldApellidos))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jComboBoxCasado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(2, 2, 2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxCasado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonAgregar.setIcon(new javax.swing.ImageIcon("C:\\Users\\brajan\\Documents\\NetBeansProjects\\UD_3_DI_Ejercicios\\src\\UD_3_DI_Ejercicio_12_aplicacion_agregar_campos\\agregar.png")); // NOI18N
        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(this);

        jButtonEliminar.setIcon(new javax.swing.ImageIcon("C:\\Users\\brajan\\Documents\\NetBeansProjects\\UD_3_DI_Ejercicios\\src\\UD_3_DI_Ejercicio_12_aplicacion_agregar_campos\\borrar.png")); // NOI18N
        jButtonEliminar.setText("Eliminar");
        jButtonEliminar.addActionListener(this);

        jButtonEditar.setIcon(new javax.swing.ImageIcon("C:\\Users\\brajan\\Documents\\NetBeansProjects\\UD_3_DI_Ejercicios\\src\\UD_3_DI_Ejercicio_12_aplicacion_agregar_campos\\editar.png")); // NOI18N
        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(this);

        jButtonCancelar.setIcon(new javax.swing.ImageIcon("C:\\Users\\brajan\\Documents\\NetBeansProjects\\UD_3_DI_Ejercicios\\src\\UD_3_DI_Ejercicio_12_aplicacion_agregar_campos\\cancelar.png")); // NOI18N
        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(this);

        jButtonGuardar.setIcon(new javax.swing.ImageIcon("C:\\Users\\brajan\\Documents\\NetBeansProjects\\UD_3_DI_Ejercicios\\src\\UD_3_DI_Ejercicio_12_aplicacion_agregar_campos\\guardar.png")); // NOI18N
        jButtonGuardar.setText("Guardar");
        jButtonGuardar.addActionListener(this);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelar)
                .addGap(5, 5, 5)
                .addComponent(jButtonAgregar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEliminar)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelar)
                    .addComponent(jButtonEliminar)
                    .addComponent(jButtonAgregar)
                    .addComponent(jButtonEditar)
                    .addComponent(jButtonGuardar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Datos Personales:");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 11, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }

    // Code for dispatching events from components to event handlers.

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource() == jButtonAgregar) {
            VentanaAplicaccion.this.jButtonAgregarActionPerformed(evt);
        }
        else if (evt.getSource() == jButtonEliminar) {
            VentanaAplicaccion.this.jButtonEliminarActionPerformed(evt);
        }
        else if (evt.getSource() == jButtonEditar) {
            VentanaAplicaccion.this.jButtonEditarActionPerformed(evt);
        }
        else if (evt.getSource() == jButtonCancelar) {
            VentanaAplicaccion.this.jButtonCancelarActionPerformed(evt);
        }
        else if (evt.getSource() == jButtonGuardar) {
            VentanaAplicaccion.this.jButtonGuardarActionPerformed(evt);
        }
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxCasadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCasadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxCasadoActionPerformed

    private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarActionPerformed
        guardarPersona();        
        jButtonAgregar.setVisible(true);
        jButtonEliminar.setVisible(true);
        jButtonEditar.setVisible(true);
        jButtonCancelar.setVisible(false);
        jButtonGuardar.setVisible(false);
    }//GEN-LAST:event_jButtonGuardarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        jButtonAgregar.setVisible(true);
        jButtonEliminar.setVisible(true);
        jButtonEditar.setVisible(true);
        jButtonCancelar.setVisible(false);
        jButtonGuardar.setVisible(false);
         //------------------------------------------------------------
        //PONER EN BLANCO LOS JTEXT
        jTextFieldNombre.setText("");
        jTextFieldApellidos.setText("");
        jTextFieldEdad.setText("");
        jTextFieldProfesion.setText("");
        jComboBoxCasado.setSelectedIndex(0);//aqui envio una posicion del Jcombobox para que no salten excepciones
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        editarPersona();        
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarActionPerformed
        eliminarPersona();
        jButtonAgregar.setVisible(true);
        jButtonEliminar.setVisible(true);
        jButtonEditar.setVisible(true);
        jButtonCancelar.setVisible(false);
        jButtonGuardar.setVisible(false);
    }//GEN-LAST:event_jButtonEliminarActionPerformed

    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed
        agregarPersona();        
        jButtonAgregar.setVisible(true);
        jButtonEliminar.setVisible(true);
        jButtonEditar.setVisible(true);
        jButtonCancelar.setVisible(false);
        jButtonGuardar.setVisible(false);
        jPanel3.setVisible(true);
    }//GEN-LAST:event_jButtonAgregarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
         
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
              
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaAplicaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaAplicaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaAplicaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaAplicaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaAplicaccion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgregar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonGuardar;
    private javax.swing.JComboBox<String> jComboBoxCasado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextFieldApellidos;
    private javax.swing.JTextField jTextFieldEdad;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldProfesion;
    // End of variables declaration//GEN-END:variables


}
