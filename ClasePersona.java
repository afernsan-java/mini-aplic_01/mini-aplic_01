/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UD_3_DI_Ejercicio_12_aplicacion_agregar_campos;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public class ClasePersona {    
    private String nombre;
    private String apellidos;
    private int edad;
    private String profesion;
    private boolean casado;   
    public ClasePersona(String nombre, String apellidos, int edad, String profesion, boolean casado) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.profesion = profesion;
        this.casado = casado;
    }  
    public String getNombre() {        
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edadEntrada) {
       
        this.edad = edadEntrada;
    }
    public String getProfesion() {
        return profesion;
    }
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    public boolean getCasado() {
        return casado;
    }
    public void setCasado(boolean casado) {
        this.casado=casado; 
    } 
}
